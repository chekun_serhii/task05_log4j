package com.chekushka.model;

import java.io.Serializable;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.PatternLayout;

@Plugin(name="SMS", category="Core", elementType="appender", printObject=true)
public final class SmsAppender extends AbstractAppender {
    public SmsAppender(String name, Filter filter,
                       Layout<? extends Serializable> layout, final boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
    }

    @Override
    public void append(LogEvent logEvent) {
        try {
            TestSMS.send(new String( getLayout().toByteArray(logEvent)));
        } catch (Exception ex) {}
    }

    @PluginFactory
    public static SmsAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginAttribute("otherAttribute") String otherAttribute) {
        if (name == null) {
            LOGGER.error("No name provided for MyCustomAppenderImpl");
            return null;
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout(); }
        return new SmsAppender(name, filter, layout, true);
    }
}
