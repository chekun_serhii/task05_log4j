package com.chekushka;

import com.chekushka.view.AppView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {
    public static void main(String[] args) {
        AppView start = new AppView();
        start.show();
    }
}
